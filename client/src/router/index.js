import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import jwt_decode from "jwt-decode";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      beforeEnter: (to, from, next) => {
        console.log("storage cleared")
        localStorage.clear();
        next()
      },
    },
    {
      path: '/waiting-room',
      name: 'WaitingRoom',
      component: () => import('../views/WaitingRoomView.vue'),
    },
    {
      path: '/game/:gameId',
      name: 'GameView',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/GameView.vue')
    },
  ]
})


export default router
