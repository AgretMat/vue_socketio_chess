"use strict";

const express = require('express');

const router = express.Router();

const chessController = require('../controllers/chessController');

router.post("/start/", chessController.createGame);

router.get("/game/:gameId", chessController.getGame);

router.put("/game/:gameId/update", chessController.updateBoard);

router.put("/game/:gameId/gameOver", chessController.gameOver);

module.exports = router;