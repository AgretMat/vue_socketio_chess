"use strict";

const express = require('express');

const router = express.Router();

const chessController = require('../controllers/authController');

router.post("/login", chessController.login);

module.exports = router;