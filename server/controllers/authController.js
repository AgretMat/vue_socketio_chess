"use strict";

const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();


exports.login = async (req, res, next) => {
    const name = req.body.name;
    const token = jwt.sign({
        name: name,
    },
        "process.env.JWT_SECRET_PHRASE", {
        expiresIn: '2h'
    }
    );
    return res.status(200).json({
        token: token
    });
};