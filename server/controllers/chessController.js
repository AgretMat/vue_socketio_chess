"use strict";

const ChessGame = require('../models/chessGame');

exports.getGame = (req, res, next) => {
    const chessGameId = req.params.gameId;
    ChessGame.findById(chessGameId)
    .then(chessGame => {
        res.status(201).json({
            chessGame: chessGame
        });
    });
};

exports.createGame = (req, res, next) => {
    const white = req.body.white;
    const black = req.body.black;

    const partie = new ChessGame({
        whiteId: white,
        blackId: black,
        isOver: false
    });
    partie.save()
    .then(res.status(200).json({
        partie: partie
    }));
    
};

exports.updateBoard = (req,res,next) => {
    const chessGameId = req.params.gameId;
    
    const fen = req.body.fen;

    ChessGame.findById(chessGameId)
    .then(chessGame => {
        chessGame.fen = fen;
        chessGame.save();
    })
    .then(chessGame => {
        res.status(201).json({
            chessGame: chessGame
        });
    })
    .catch(err => {
        res.status(404).json({
            error: err,
            message: "Game does not exist"
        });
    })
};

exports.gameOver = (req,res,next) => {
    const chessGameId = req.params.gameId;

    const winner = req.body.winner;
    const loser =  req.body.loser;
    const isDraw = req.body.isDraw;

    ChessGame.findById(chessGameId)
    .then(chessGame => {
        chessGame.isOver = true;
        chessGame.winner = winner;
        chessGame.loser = loser;
        chessGame.isDraw = isDraw;
        chessGame.save();
    })
    .then(chessGame => {
        res.status(201).json({
            chessGame: chessGame
        });
    })
    .catch(err => {
        res.status(404).json({
            error: err,
            message: "Game does not exist"
        });
    })
}