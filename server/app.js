"use strict";

// Initialisation de l'app et de mongoose.
const axios = require('axios');
const express = require('express');
const mongoose = require('mongoose');
const { createServer } = require("http");
const { Server } = require("socket.io");
const cors = require("cors");

const app = express();

const routes = [
    require('./routes/chess'),
    require('./routes/auth'),

    // Ajouter les nouvelles routes ici
  ];

require('dotenv').config();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
  });

app.use('/api', routes);


// Connexion à la base de données
mongoose
  .connect("mongodb+srv://jcu:admin@cluster0.v3tbx.mongodb.net/VueChessSocket?retryWrites=true&w=majority");

  const httpServer = createServer(app);
  const io = new Server(httpServer, {
    cors: {
      origin: '*',
    }
  });

let waitingRoom = [];
let dictGames = {};


io.on('connection', socket => {

  socket.on('addToWaitingRoom', name => {
    if (!waitingRoom.includes(name)) {

      waitingRoom.push(name);
      console.log(`Added ${name} to waiting room. Waiting room size: ${waitingRoom.length}`);
      if (waitingRoom.length >= 2) {
        const user1 = waitingRoom.shift();
        const user2 = waitingRoom.shift();

        let postData = {
          black: user2,
          white: user1,
        };

        let axiosConfig = {
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            "Access-Control-Allow-Origin": "*",
          }
        };

        axios.post('http://localhost:3000/api/start', postData, axiosConfig)
          .then(response => {
            io.emit("gameStart", user1, user2, response.data.partie._id)
          });
      }
    }
  });

  socket.on('removeFromWaitingRoom', name => {
    waitingRoom = waitingRoom.filter(user => user !== name)
    console.log(`Removed ${name} from waiting room. Waiting room size: ${waitingRoom.length}`);
  })


});

// Port du socket
httpServer.listen(PORT, () => {
    console.log('http server listening on ', PORT);
  });