const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChessGame = new Schema({
    whiteId: {
        type: String,
        required: true,
    },
    blackId: {
        type: String,
        required: true,
    },
    isOver: {
        type: Boolean,
        required: true
    },
    winner: {
        type: String,
        required: false
    },
    loser: {
        type: String,
        required: false
    },
    isDraw: {
        type: Boolean,
        required: false
    },
    fen: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ChessGame', ChessGame);